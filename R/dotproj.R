#' Calculates projection of a point onto a line with a given slope.
#' 
#' @param x X coordinate. Numeric, required.
#' @param y Y coordinate. Numeric, required.
#' @param direction. Rotation degree. Numeric from 0 to 1, required.
#' @return Numeric, distance from (0,0) to projection point.
#' @export

dotproj <- function(x = NULL, y = NULL, direction = NULL) {
  
  stopifnot(!is.null(x))
  stopifnot(!is.null(y))
  stopifnot(length(x) == length(y))
  stopifnot(!is.null(direction))
  stopifnot(length(direction) == 1)
  stopifnot(direction >= 0 & direction <= 1)
  
  angle <- pi / 2 * direction
  
  slope <- tan(angle)
  
  x2 <- (x / slope - y) / (slope + 1 / slope)
  y2 <- slope * x2
  sqrt(x2 ^ 2 + y2 ^ 2)
  
}
